<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});
Route::resource('customers', 'CustomersController')->middleware('auth');
//Route::get('customers/delete/{id}', 'CustomersController@destroy')->name('customer.delete')->middleware('auth');
Route::get('customers/changestatus/{cid}/{sid}', 'CustomersController@changeStatus')->name('customers.changestatus')->middleware('auth');
Route::get('activecustomers', 'CustomersController@active')->name('customers.active')->middleware('auth');

Route::resource('receipts', 'ReceiptsController')->middleware('auth');
Route::get('receipts/delete/{id}', 'ReceiptsController@destroy')->name('receipt.delete')->middleware('auth');
Route::get('receipts', 'ReceiptsController@index')->middleware('auth');
Route::get('myreceipts', 'ReceiptsController@myReceipts')->name('receipts.myreceipts')->middleware('auth');
Route::get('payment/{id}', 'ReceiptsController@payment')->name('receipts.payment')->middleware('auth');
Route::post('multiplerecords', 'MeetingsController@multiplerecords')->name('meetings.multiplerecords')->middleware('auth');

Route::resource('users', 'UsersController')->middleware('auth');
Route::get('users/delete/{id}', 'UsersController@destroy')->name('user.delete')->middleware('auth');
Route::post('users/{id}/update/', 'UsersController@update')->name('users.update')->middleware('auth');
Route::get('users/changecustomer/{uid}/{cid?}', 'UsersController@changeCustomer')->name('user.changecustomer')->middleware('auth');
Route::get('editadmin','UsersController@editAdmin')->name('user.editadmin')->middleware('auth');


Route::resource('suppliers', 'SuppliersController')->middleware('auth');
//Route::get('suppliers/delete/{id}', 'SuppliersController@destroy')->name('supplier.delete')->middleware('auth');
Route::get('suppliers/changestatus/{cid}/{sid}', 'SuppliersController@changeStatus')->name('suppliers.changestatus')->middleware('auth');
Route::get('activesuppliers', 'SuppliersController@active')->name('suppliers.active')->middleware('auth');

Route::resource('meetings', 'MeetingsController')->middleware('auth');
Route::get('meetings/delete/{id}', 'MeetingsController@destroy')->name('meeting.delete')->middleware('auth');
Route::patch('meetings/{id}/update/', 'MeetingsController@update')->name('meetings.update')->middleware('auth');
Route::get('personal', 'MeetingsController@personal')->name('meetings.personal')->middleware('auth');
Route::get('business', 'MeetingsController@business')->name('meetings.business')->middleware('auth');
Route::get('add','MeetingsController@add')->name('meetings.add')->middleware('auth');
Route::get('meetings/{id}/close/','MeetingsController@close')->name('meetings.close')->middleware('auth');
Route::get('mymeetings', 'MeetingsController@myMeetings')->name('meetings.mymeetings')->middleware('auth');
Route::post('multipledelete', 'MeetingsController@multipledelete')->name('meetings.multipledelete')->middleware('auth');
Route::get('closepersonal/{id}','MeetingsController@closePersonal')->name('closepersonal')->middleware('auth');
Route::get('schedule', 'MeetingsController@schedule')->name('meetings.schedule')->middleware('auth');
Route::get('search', 'MeetingsController@search')->name('meetings.search')->middleware('auth');
Route::get('mydebts', 'MeetingsController@myDebts')->name('meetings.mydebts')->middleware('auth');
Route::get('searchcustomer', 'MeetingsController@searchCustomer')->name('meetings.searchcustomer')->middleware('auth');
Route::get('debts', 'MeetingsController@debts')->name('meetings.debts')->middleware('auth');

Route::get('mail/{id}', 'MailController@mail')->name('meetings.mail')->middleware('auth');
Route::post('send/email', 'MailController@sendemail')->middleware('auth');




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


