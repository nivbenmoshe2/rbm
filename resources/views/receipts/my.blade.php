@extends('layouts.app')

@section('title', 'Receipts')

@section('content')



<div class="container">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <div class="card-header"><h2>My Receipts</h2></div>        
                    <div class="card-body">
                        <table class = "table table-striped">
                            <tr>
                                <th>Reference No</th><th>Customer</th><th>Total</th><th>date</th><th></th>
                            </tr>
                            <!-- the table data -->
                            @foreach($receipts as $receipt)
                                    <tr>
                                        <td>{{$receipt->number}}</td>
                                        <td>{{$receipt->customer->name}}</td>
                                        <td>{{$receipt->cash+$receipt->bit}}</td>
                                        <td>{{date('d-m-Y', strtotime($receipt->date))}}</td>  
                                        <td>
                                            <a class="btn btn-primary btn-sm" href = "{{route('receipts.show',$receipt->id)}}">Details</a>
                                        </td>                                                              
                                    </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
@endsection

