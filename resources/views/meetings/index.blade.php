@extends('layouts.app')

@section('title', 'Meetings')

@section('content')



<div class="container">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <h2>All Meetings</h2>
                </div> 
                     
                    <div class="card-body">
                        <form method="GET" action="{{route('meetings.search')}}">
                            <div class="row">
                                <div class="col-md-3">
                                    <input type = "date" class="form-control" name = "from" placeholder="From Date">
                                </div>
                                <div class="input-group-addon">
                                    To
                                </div>
                                <div class="col-md-3">
                                    <input type = "date" class="form-control" name = "to" placeholder="To Date">
                                </div>
                                <div class="col-md-3">
                                    <button type="submit" class="btn btn-info btn-sm"><i class="fa fa-search"></i></button>
                                    <a href = "{{url('meetings')}}" class="btn btn-info btn-sm">All</a>
                                </div> 
                                
                            </div> 
                        </form>  
                        <form method="post" action="{{url('multipledelete')}}">
                            {{ csrf_field() }}
                            <div class="box-body">
                                @if(Session::has("Delete"))
                                    <div class="alert alert-success">
                                        <b>Successfull, The meeting has been deleted.</b>
                                    </div>
                                @endif
                        <table class = "table table-striped">
                            <tr>
                                <th><input type="submit" onclick="return confirm('Are you sure you would like to delete there meetings?');" class="btn btn-danger" name="submit" value="Delete"/></th><th>Type</th><th>Place</th><th>Date</th><th>Start</th><th>End</th><th>Close</th><th></th>
                            </tr>
                            <!-- the table data -->
                            @foreach($meetings as $meeting)
                                <tr>
                                    <td><input name='id[]' type="checkbox" id="checkItem" value="{{$meeting->id}}">
                                    <td>{{$meeting->type}}</td>
                                    <td>{{$meeting->place}}</td>
                                    <td>{{date('d-m-Y', strtotime($meeting->date))}}</td>
                                    <td>{{date('H:i', strtotime($meeting->start))}}</td>
                                    <td>{{date('H:i', strtotime($meeting->end))}}</td>
                                    <td>
                                        @if(!isset($meeting->summary))
                                            @can('admin')
                                                @if($meeting->type == 'Business')
                                                    <a href = "{{route('meetings.close',$meeting->id)}}" class="btn btn-primary btn-sm">Close</a>
                                                @else
                                                    <a href = "{{route('closepersonal',$meeting->id)}}" class="btn btn-primary btn-sm">Close</a>
                                                @endif
                                            @endcan
                                        @else
                                            <h5><small class="badge badge-success" role="alert"><strong>Done</strong></small></h5>
                                        @endif
                                    </td> 
                                    <td class="text-right">
                                        <div class="dropdown">
                                        <a class="btn btn-sm btn-icon-only text-dark" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                                            <i class="fas fa-ellipsis-v"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                            <a class="dropdown-item" href="{{route('meetings.show',$meeting->id)}}">Show</a>
                                            <a class="dropdown-item" href="{{route('meetings.edit',$meeting->id)}}">Edit</a>
                                            <a class="dropdown-item" onclick="return confirm('Are you sure you would like to delete this meeting?');" href="{{route('meeting.delete',$meeting->id)}}">Delete</a>  
                                        </div>
                                        </div>
                                    </td>                                                         
                                </tr>
                            @endforeach
                        </table>
                    </div>
                        </form>
                        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"> </script>
                        <script language="javascript">
                        $("#checkAll").click(function () {
                        $('input:checkbox').not(this).prop('checked', this.checked);
                        });
                        </script>
                        {{$meetings->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
@endsection

