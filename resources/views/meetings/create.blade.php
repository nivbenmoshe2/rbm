@extends('layouts.app')

@section('title', 'Create meeting')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create business meeting</div>        
                    <div class="card-body">
                        <form method = "post" action = "{{action('MeetingsController@store')}}">
                        @csrf 
                        
                        <div class="form-group">
                            <label for = "place">Place</label>
                            <input type = "text" class="form-control @error('place') is-invalid @enderror" name = "place" required autocomplete="place" autofocus>
                            @error('place')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div> 
                        <div class="form-group">
                            <label for = "date">Date</label>
                            <input type = "date" class="form-control @error('date') is-invalid @enderror" name = "date" required autocomplete="date" autofocus>
                            @error('date')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                        </div>
                        <div class="form-group">
                            <label for = "start">Start</label>
                            <input type = "time" class="form-control @error('start') is-invalid @enderror" name = "start" required autocomplete="start" autofocus>
                            @error('start')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                        </div> 
                        <div class="form-group">
                            <label for = "end">End</label>
                            <input type = "time" class="form-control @error('end') is-invalid @enderror" name = "end" required autocomplete="end" autofocus>
                            @error('end')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>     
                        <div class="form-group">
                            <label for = "equipment">Equipment</label>
                            <input type = "text" class="form-control" name = "equipment">
                            
                        </div>
                        <div class="form-group" >
                            <label for="service_id">Service</label>
                                <select class="form-control" name="service_id">                                                                          
                                @foreach ($services as $service)
                                    <option value="{{ $service->id }}"> 
                                        {{ $service->name }} 
                                    </option>
                                @endforeach    
                                </select>
                        </div>
                        <div class="form-group" >
                            <label for="customer_id">Customer</label>
                                <select class="form-control" name="customer_id">                                                                          
                                @foreach ($customers as $customer)
                                    <option value="{{ $customer->id }}"> 
                                        {{ $customer->name }} 
                                    </option>
                                @endforeach    
                                </select>
                        </div>
                        <div class="form-group" >
                            <label for="supplier_id">Supplier</label>
                                <select class="form-control" name="supplier_id">  
                                    <option selected></option>                                                                        
                                @foreach ($suppliers as $supplier)
                                    <option value="{{ $supplier->id }}"> 
                                        {{ $supplier->name }} 
                                    </option>
                                @endforeach    
                                </select>
                        </div>
                        
                        <div> 
                            <input TYPE="button" class="btn btn-primary" VALUE="Back" onClick="history.go(-1);">
           
                            <input type = "submit" class="btn btn-primary" name = "submit" value = "Create meeting">
                        </div>                       
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    
@endsection
