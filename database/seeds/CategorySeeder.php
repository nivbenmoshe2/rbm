<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'name' => 'Furniture and Accessories',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Sanitary',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],  
            [
                'name' => 'Lighting',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ], 
            [
                'name' => 'Kitchens',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Ceramics',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],                 
            ]);
    }
}
