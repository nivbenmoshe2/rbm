<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Receipt;
use App\Customer;
use App\Meeting;
use App\User;
use App\Supplier;
use App\Service;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;

class ReceiptsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Gate::authorize('admin');
        $receipts = Receipt::orderBy('created_at', 'DESC')->paginate(5);
        $customers = Customer::all();
        return view('receipts.index', compact('receipts','customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Gate::authorize('admin');
        $customers = Customer::where('status_id',1)->get();
        return view('receipts.create', compact('customers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $receipt = new Receipt();
        $rec = $receipt->create($request->all());
        $rec->save();
        return redirect('receipts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $receipt = Receipt::findOrFail($id);
        return view('receipts.show', compact('receipt'));
    }

    public function payment()
    {
        Gate::authorize('admin');
        $url = substr(str_replace(url('/payment'), '', url()->current()),1);
        $cid = Receipt::where('id',$url)->value('customer_id');
        $meetings = Meeting::where('customer_id',$cid)->whereNotNull('summary')->whereNull('receipt_id')->paginate(5);
        $suppliers = Supplier::all();
        $receipts = Receipt::all();  
        $customers = Customer::all();      
        $services = Service::all();     
        return view('meetings.pay', compact('meetings','suppliers','receipts','customers','services'));
    }

    

    public function myReceipts()
    {        
        $userId = Auth::id();
        $user = User::findOrFail($userId);
        $customer = $user->customer;
        $receipts = $customer->receipts;
        return view('receipts.my', compact('receipts','customer')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Gate::authorize('admin');
        $receipt = Receipt::findOrFail($id);
        $customers = Customer::where('status_id',1)->get();
        return view('receipts.edit', compact('receipt','customers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $receipt = Receipt::findOrFail($id);
        $receipt->update($request->all());
        return redirect('receipts'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Gate::authorize('admin');
        $receipt = Receipt::findOrFail($id);
        $receipt->delete(); 
        return redirect('receipts');
    }
}
