<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    public function customers()
    {
        return $this->hasMany('App\Customer');
    }

    public function suppliers()
    {
        return $this->hasMany('App\Supplier');
    }




}
